﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DoublyUtils
{
    public class LogUtils
    {
        //1.配置文件获取，从appSetting中获取
        //配置文件内容
        // a. Log文件存放的位置
        // b. Log输出格式

        //public static String logDir = @"C:\Users\zengyi2\Desktop";
        public static String logDir = System.Threading.Thread.GetDomain().BaseDirectory;
        private Type type;
        private static readonly String logFileName = "Clipboard.log";
        private static Dictionary<Type,LogUtils> logDic = new Dictionary<Type,LogUtils>();

        public static LogUtils getInstance(Type type)
        {
            if (!logDic.ContainsKey(type))
            {
                if (logDic.Count == 0)
                    File.Create(Path.Combine(logDir, logFileName)).Close();

                logDic[type] = new LogUtils(type);
                return logDic[type];
            }
            else
            {
                return logDic[type];
            }
        }

        private LogUtils(Type type)
        {
            this.type = type;
            //if (File.Exists(Path.Combine(logDir, logFileName)))
            //{
            //    File.Create(Path.Combine(logDir, logFileName)).Close();
            //}
        }

        public void info(String str)
        {
            DateTime now = DateTime.Now;
            String s = "[" + now.ToLocalTime().ToString() + "][" + type.FullName + "][INFO]:" + str +"\r\n";
            consoleLog(s);
            FileUtils.Write(Path.Combine(logDir, logFileName), Encoding.UTF8.GetBytes(s), true);
        }

        public void error(String str)
        {
            DateTime now = DateTime.Now;
            String s = "[" + now.ToLocalTime().ToString() + "][" + type.FullName + "][ERROR]:" + str +"\r\n";
            consoleLog(s);
            FileUtils.Write(Path.Combine(logDir, logFileName), Encoding.UTF8.GetBytes(s), true);
        }

        public void consoleLog(String s)
        {
            Console.WriteLine(s);
        }
        
    }
}
