﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DoublyUtils
{
    public class FileUtils
    {

        private static LogUtils log = LogUtils.getInstance(typeof(FileUtils));

        /// <summary>
        /// 写入文件内容
        /// </summary>
        /// <param name="path">文件路径</param>
        /// <param name="data">待写入内容</param>
        /// <param name="append">是否以追加形式写入</param>
        public static void Write(string path, byte[] data, Boolean append)
        {
            //if (!Directory.Exists(this.txtPath.Text)) return;

            //createFile();

            try
            {
                using (FileStream fs = File.Open(path, append ? FileMode.Append : FileMode.Create))
                {
                    BinaryWriter bw = new BinaryWriter(fs);

                    bw.Write(data);
                    bw.Close();
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                log.error(ex.Message);
                log.error(ex.StackTrace);
            }
        }

        /// <summary>
        /// 以覆盖或者创建的形式写入文件内容
        /// </summary>
        /// <param name="path">文件路径</param>
        /// <param name="data">待写入数据</param>
        public static void Write(String path, byte[] data)
        {
            Write(path, data, false);
        }

        /// <summary>
        /// 读取文件内容
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static byte[] Read(string path)
        {
            if (!File.Exists(path)) return null;
            byte[] data = File.ReadAllBytes(path);
            return data;
        }
    }
}
